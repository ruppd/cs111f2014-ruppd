import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;

public class TodoList {

    private ArrayList<TodoItem> todoItems;	
    private static final String TODOFILE = "todoTest.txt"; //uses scanner to read files

    public TodoList() {
        todoItems = new ArrayList<TodoItem>(); //creates instance of todoItems
    }

//method for adding a todo item 
    public void addTodoItem(TodoItem todoItem) {
        todoItems.add(todoItem);
    }

//method returns todoItem
    public Iterator getTodoItems() {
        return todoItems.iterator();
    }

//code for the command 'read'
    public void readTodoItemsFromFile() throws IOException {
        Scanner fileScanner = new Scanner(new File(TODOFILE)); //reads file
	//reads items one at a time, assigns priority, category, and task
        while(fileScanner.hasNext()) {
            String todoItemLine = fileScanner.nextLine();
            Scanner todoScanner = new Scanner(todoItemLine);
            todoScanner.useDelimiter(",");
            String priority, category, task;
            priority = todoScanner.next();
            category = todoScanner.next();
            task = todoScanner.next();
            TodoItem todoItem = new TodoItem(priority, category, task);
            todoItems.add(todoItem);
        }
    }

//method to mark a task 'done'
    public void markTaskAsDone(int toMarkId) {
        Iterator iterator = todoItems.iterator();
        while(iterator.hasNext()) {
            TodoItem todoItem = (TodoItem)iterator.next();
            if(todoItem.getId() == toMarkId) {
                todoItem.markDone();
            }
        }
    }

//code for priority-search command
    public Iterator findTasksOfPriority(String requestedPriority) {
        ArrayList<TodoItem> priorityList = new ArrayList<TodoItem>();
        // TODO: Add source code that will find and return all tasks of the requestedPriority
        Iterator iterator = todoItems.iterator();
        while(iterator.hasNext()) {
	    TodoItem todoItem = (TodoItem)iterator.next();
	    if(todoItem.getPriority().equals(requestedPriority)) {
		priorityList.add(todoItem);
	    }
	}
        return priorityList.iterator();

    }

//code for category-search command
    public Iterator findTasksOfCategory(String requestedCategory) {
        ArrayList<TodoItem> categoryList = new ArrayList<TodoItem>();
        // TODO: Add source code that will find and return all tasks for the requestedCategory
        Iterator iterator = todoItems.iterator();
        while(iterator.hasNext()) {
	    TodoItem todoItem = (TodoItem)iterator.next();
	    if(todoItem.getCategory().equals(requestedCategory)) {
		categoryList.add(todoItem);
	    }
	}
        return categoryList.iterator();
    }

//converts todo items to String
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        Iterator iterator = todoItems.iterator();
        while(iterator.hasNext()) {
            buffer.append(iterator.next().toString());
            if(iterator.hasNext()) {
                buffer.append("\n");
            }
        }
        return buffer.toString();
    }

}
