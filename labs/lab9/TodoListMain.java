import java.io.IOException;
import java.util.Iterator;
import java.util.Scanner;

public class TodoListMain {

    public static void main(String[] args) throws IOException {
        System.out.println("Welcome to the Todo List Manager!");
        System.out.println("What operation would you like to perform?");
        System.out.println("Available options: read, priority-search, category-search, done, list, quit");

        Scanner scanner = new Scanner(System.in);
        TodoList todoList = new TodoList();

        while(scanner.hasNext()) {
            String command = scanner.nextLine();
            if(command.equals("read")) {
                todoList.readTodoItemsFromFile();
            }
            else if(command.equals("list")) {
                System.out.println(todoList.toString());
            }
            else if(command.equals("done")) {
                System.out.println("What is the id of the task?");
                int chosenId = scanner.nextInt();
                todoList.markTaskAsDone(chosenId);
            }
            else if(command.equals("quit")) {
                break;
            }
	//add operations priority search and category search
	    else if(command.equals("priority-search")) {
		System.out.println("What is the priority you are looking for?");
		String requestedPriority = scanner.next();
		Iterator priorityList = todoList.findTasksOfPriority(requestedPriority);
            	while(priorityList.hasNext()) {
		    System.out.println(priorityList.next());
		}
	    }
	    else if(command.equals("category-search")){
		System.out.println("What is the category you are looking for?");
		String requestedCategory = scanner.next();
		Iterator categoryList = todoList.findTasksOfCategory(requestedCategory);
            	while(categoryList.hasNext()) {
		    System.out.println(categoryList.next());
		}
	    }
//	    else {
//		System.out.println("Command not recognized.");
//	    }
        }


    }

}
