
// *********************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Darby Rupp
// CMPSC 111 Fall 2014
// Lab 6
// Date: October 9 2014
// 
// Purpose: Periodic Compound Interest formula
// ********************************

import java.util.Date; 
import java.util.Scanner;
import java.text.NumberFormat;
import java.util.Random;

public class Lab6
{
	public static void main(String[] args)
	{
		//Label w name and date
		System.out.println("Darby Rupp\nLab 6\n" + new Date() + "\n");

		//Variable dictionary
		Scanner scan = new Scanner(System.in);
		NumberFormat money = NumberFormat.getCurrencyInstance();
		NumberFormat percent = NumberFormat.getPercentInstance();
		Random rand = new Random();
		double P, n, t, r, A, working;
		
		//program
		//getting values from user
		System.out.print("Enter the amount of money borrowed/deposited: \033[1m");
		P = scan.nextDouble();
		System.out.print("\033[0mEnter the number of years for which the amount is borrowed/deposited: \033[1m");
		t = scan.nextDouble();
		System.out.print("\033[0mEnter the number of times the interest is compounded per year: \033[1m");
		n = scan.nextDouble();
		
		//calc w/random r
		r = rand.nextDouble()*14/100+.02; //0-13.9999/100+.02
		A = P * Math.pow(1+r/n, n*t);
		
		//checking
		System.out.print("\033[0mInitial amount of \033[1m"+money.format(P)+"\033[0m");	//print P
		System.out.print(" after \033[1m"+t+"\033[0m  years,");				//print t
		System.out.print(" compounded \033[1m"+n+"\033[0m times per year = ");		//print n
		System.out.println("\033[1m"+money.format(A)+"\033[0m");			//print A
		System.out.println("Interest rate was: \033[1m"+percent.format(r)+"\033[0m");	//print r
		
		//continuous compounding formula:
		A = P * Math.exp(r*t);
		System.out.print("\033[0m\nInitial amount of \033[1m"+money.format(P)+"\033[0m");//print P
		System.out.print(" compounded continuously for \033[1m"+t+"\033[0m  years,");	//print t
		System.out.print(" at a rate of \033[1m"+percent.format(r)+"\033[0m = ");	//print r
		System.out.println("\033[1m"+money.format(A)+"\033[0m");			//print A
		System.out.println();


	}
}
