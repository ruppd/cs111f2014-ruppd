ruppd@aldenv173:~/cs111F2014/cs111F2014-ruppd/labs/lab6$ java Lab6 
Darby Rupp
Lab 6
Thu Oct 09 15:54:11 EDT 2014

Enter the amount of money borrowed/deposited: 150.25
Enter the number of years for which the amount is borrowed/deposited: 15
Enter the number of times the interest is compounded per year: 4
Initial amount of $150.25 after 15.0  years, compounded 4.0 times per year = $1,136.30
Interest rate was: 14%

Initial amount of $150.25 compounded continuously for 15.0  years, at a rate of 14% = $1,176.18

