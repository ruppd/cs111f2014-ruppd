// *********************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Darby Rupp
// CMPSC 111 Fall 2014
// Lab 4
// Date: Sept 25 2014
// 
// Purpose: Create a Java Masterpiece Drawing
// ********************************

import javax.swing.*;

public class Lab4Test
{
	public static void main(String[] args)
	{
        	JFrame window = new JFrame("Darby Rupp Lab 4 Drawing");

        	window.getContentPane().add(new Lab4Drawing());
        	window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        	window.setVisible(true);
		window.setSize(600, 400);

		
	}
}
