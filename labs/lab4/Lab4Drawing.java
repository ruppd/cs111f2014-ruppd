// *********************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Darby Rupp
// CMPSC 111 Fall 2014
// Lab 4
// Date: Sept 25 2014
// 
// Purpose: Create a Java Masterpiece Drawing
// ********************************

import java.awt.*;
import javax.swing.JApplet;

public class Lab4Drawing extends JApplet
{
    //-------------------------------------------------
    // Use Graphics methods to add content to the drawing canvas
    //-------------------------------------------------
    public void paint(Graphics p)
    {
	final int WIDTH = 600;
	final int HEIGHT = 400;
	Color brown = new Color(135,110,50);
	
	//background
	p.setColor(Color.lightGray);
	p.fillRect(0, 0, WIDTH, HEIGHT);
	//computer monitor
	p.setColor(Color.gray);
	p.fillRect(75, 75, 420, 220);
	//computer base
	p.fillRect(260, 295, 50, 50);
	p.fillRect(200, 345, 170, 10);
	//computer screen
	p.setColor(Color.darkGray);
	p.fillRect(95, 95, 380, 180);
	//desk
	p.setColor(brown);
	p.fillRect(0, HEIGHT-45, WIDTH, 45);
	//Words; WarGames quote
	p.setColor(Color.green);
	p.drawString("Do you want to play a game?", 115, 120);
	p.drawString("\"How about... GlobalThermonuclearWar?\"", 115, 150);
	p.drawString(" - WarGames (1983)", 215, 190);
	//smile
	p.fillOval(400, 200, 5, 5);
	p.fillOval(420, 200, 5, 5);
	p.drawArc(393, 210, 40, 20, 0, -180);
    }
}
