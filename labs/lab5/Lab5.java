
// *********************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Darby Rupp (and Jonathan Yee)
// CMPSC 111 Fall 2014
// Lab 4
// Date: October 2 2014
// 
// Purpose: DNA Sequence changes
// ********************************

import java.util.Date; //needed to print today's date
import java.util.Random;
import java.util.Scanner;

public class Lab5
{
	public static void main(String[] args)
	{
		//Label 
		System.out.println("Darby Rupp and Jonathan Yee\nLab 4\n" + new Date() + "\n");

		//Variable dictionary
		Scanner scan = new Scanner(System.in);
		String dnaString;
		String dnaComplement;
		String dnaInsert;
		String dnaDel;
		Random mut = new Random();
		int length;
		int location;
		char base;

		//Program
		System.out.println("Enter a string containing only C, G, T, and A: ");
		dnaString = scan.nextLine();
		dnaString = dnaString.toLowerCase(); //input in lowercase (just in case!)

		//Complementary Strand
		dnaComplement = dnaString.replace('a', 'T');
		dnaComplement = dnaComplement.replace('t', 'A');
		dnaComplement = dnaComplement.replace('c', 'G');
		dnaComplement = dnaComplement.replace('g', 'C');
		dnaString = dnaString.toUpperCase(); //now replace done, want original to be uppercase for remainder of program
		System.out.println("Complement of "+dnaString+" is "+dnaComplement);

		//Mutations: Insertion
		length = dnaString.length();
		location = mut.nextInt(length+1);
		base = "ATCG".charAt(mut.nextInt(4));
		dnaInsert = dnaString.substring(0,location)+base+dnaString.substring(location);
		System.out.println("Inserting "+base+" at position "+location+" gives "+dnaInsert);

		//Mutation: Deleting
		length = dnaString.length();
		location = mut.nextInt(length+1);
		dnaDel = dnaString.substring(0,location)+dnaString.substring(location+1);
		System.out.println("Deleting from position "+location+" gives "+dnaDel);

		//Mutation: Changing
		length = dnaString.length();
		location = mut.nextInt(length+1);
//		if (location = length) {
//			location = mut.nextInt(length); }
		base = "ATCG".charAt(mut.nextInt(4));
		dnaDel = dnaString.substring(0,location)+base+dnaString.substring(location+1);
		System.out.println("Changing position "+location+" gives "+dnaDel);

	}
}
