// *********************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Darby Rupp
// CMPSC 111 Fall 2014
// Lab 8
// Date: Oct 30 2014
//
// Purpose: Sudoku Checker Main arg
// ********************************

import java.util.Date;

public class SudokuCheckerMain
{
	//main method, program begins here
	public static void main(String[] args)
	{
		SudokuChecker checker = new SudokuChecker();

		//Welcome message
		System.out.println("Darby Rupp\nLab 8\n" + new Date() + "\n");
		System.out.println("Welcome to the Sudoku Checker!\n");
		System.out.println("This program checks simple and small 4x4 Sudoku grids for correctness.");
		System.out.println("Each column, row, and 2x2 region contains the numbers 1 through 4 only once.\n");
		System.out.println("To check your Sudoku, enter your board one row at a time, ");
		System.out.println("with each digit separated by a space.  Hit ENTER at the end of a row.\n");

		checker.getGrid();
		checker.checkGrid();

	}
}
