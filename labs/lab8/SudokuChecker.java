// *********************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Darby Rupp
// CMPSC 111 Fall 2014
// Lab 8
// Date: Oct 30 2014
//
// Purpose: SudokuChecker
// ********************************

import java.util.Scanner;

public class SudokuChecker
{
    private int w1, w2, w3, w4, x1, x2, x3, x4, y1, y2, y3, y4, z1, z2, z3, z4;
    boolean isGood = true;
    Scanner input = new Scanner(System.in);

    public SudokuChecker()
    {
	w1 = 0;
	w2 = 0;
	w3 = 0;
	w4 = 0;
	x1 = 0;
	x2 = 0;
	x3 = 0;
	x4 = 0;
	y1 = 0;
	y2 = 0;
	y3 = 0;
	y4 = 0;
	z1 = 0;
	z2 = 0;
	z3 = 0;
	z4 = 0;
    }

    public void getGrid()
    {
	System.out.print("Enter row 1: ");
	w1 = input.nextInt();
	w2 = input.nextInt();
	w3 = input.nextInt();
	w4 = input.nextInt();

	System.out.print("Enter row 2: ");
	x1 = input.nextInt();
	x2 = input.nextInt();
	x3 = input.nextInt();
	x4 = input.nextInt();

	System.out.print("Enter row 3: ");
	y1 = input.nextInt();
	y2 = input.nextInt();
	y3 = input.nextInt();
	y4 = input.nextInt();

	System.out.print("Enter row 4: ");
	z1 = input.nextInt();
	z2 = input.nextInt();
	z3 = input.nextInt();
	z4 = input.nextInt();
    }

    public void checkGrid()
    {
	//check rows
	System.out.println();
	System.out.print("ROW-1:");
	if ((w1+w2+w3+w4)==10)
	{
		System.out.println("GOOD");
	}
	else
	{
		System.out.println("INCORRECT");
		isGood = false;
	}

	System.out.print("ROW-2:");
	if ((x1+x2+x3+x4)==10)
	{
		System.out.println("GOOD");
	}
	else
	{
		System.out.println("INCORRECT");
		isGood = false;
	}

	System.out.print("ROW-3:");
	if ((y1+y2+y3+y4)==10)
	{
		System.out.println("GOOD");
	}
	else
	{
		System.out.println("INCORRECT");
		isGood = false;
	}

	System.out.print("ROW-4:");
	if ((z1+z2+z3+z4)==10)
	{
		System.out.println("GOOD");
	}
	else
	{
		System.out.println("INCORRECT");
		isGood = false;
	}

	//check columns
	System.out.println();
	System.out.print("COL-1:");
	if ((w1+x1+y1+z1)==10)
	{
		System.out.println("GOOD");
	}
	else
	{
		System.out.println("INCORRECT");
		isGood = false;
	}

	System.out.print("COL-2:");
	if ((w2+x2+y2+z2)==10)
	{
		System.out.println("GOOD");
	}
	else
	{
		System.out.println("INCORRECT");
		isGood = false;
	}

	System.out.print("COL-3:");
	if ((w3+x3+y3+z3)==10)
	{
		System.out.println("GOOD");
	}
	else
	{
		System.out.println("INCORRECT");
		isGood = false;
	}

	System.out.print("COL-4:");
	if ((w4+x4+y4+z4)==10)
	{
		System.out.println("GOOD");
	}
	else
	{
		System.out.println("INCORRECT");
		isGood = false;
	}

	//check regions
	System.out.println();
	System.out.print("REG-1:");
	if ((w1+w2+x1+x2)==10)
	{
		System.out.println("GOOD");
	}
	else
	{
		System.out.println("INCORRECT");
		isGood = false;
	}

	System.out.print("REG-2:");
	if ((y1+y2+z1+z2)==10)
	{
		System.out.println("GOOD");
	}
	else
	{
		System.out.println("INCORRECT");
		isGood = false;
	}

	System.out.print("REG-3:");
	if ((w3+w4+x3+x4)==10)
	{
		System.out.println("GOOD");
	}
	else
	{
		System.out.println("INCORRECT");
		isGood = false;
	}

	System.out.print("REG-4:");
	if ((y3+y4+z3+z4)==10)
	{
		System.out.println("GOOD");
	}
	else
	{
		System.out.println("INCORRECT");
		isGood = false;
	}

	//whole grid
	if (isGood==true)
	{
		System.out.println("\nSUDOKU GRID IS VALID! :)\n");
	}
	else
	{
		System.out.println("\nSUDOKU GRID IS NOT VALID! :(\n");
	}

    }

}
