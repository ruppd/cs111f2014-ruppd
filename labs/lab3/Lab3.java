
// *********************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Darby Rupp
// CMPSC 111 Fall 2014
// Lab 3
// Date: Sept 18 2014
// 
// Purpose: Tip and Bill calculator
// ********************************

import java.util.Date; //needed to print today's date
import java.util.Scanner; //needed to scan
import java.text.NumberFormat; //needed to format variables easily

public class Lab3  
{
	public static void main(String[] args)
	{
		System.out.println("Darby Rupp\nLab 3\n" + new Date() + "\n");

		//declaring variables
		Scanner in = new Scanner(System.in);
		String name;
		double bill;
		double percentage;
		double tip;
		double totalBill;
		int numPeople;
		double billShare;
		NumberFormat fmt = NumberFormat.getCurrencyInstance();

		//program
		System.out.print("Please enter your name: ");
		name = in.nextLine();
		System.out.println("\nHope you enjoyed your dinner, " + name + ".");
		System.out.print("Please enter the amount of your bill: ");
		bill = in.nextDouble();
		System.out.print("Please enter the percentage that you want to tip: ");
		percentage = in.nextDouble();
		tip = (percentage/100)*bill;
		totalBill = (bill+tip);
		System.out.println("Your original bill was " + fmt.format(bill));
		System.out.println("Your tip amount is " + fmt.format(tip));
		System.out.println("Your total bill is " + fmt.format(totalBill));
		
		System.out.print("How many people will be splitting the bill? ");
		numPeople = in.nextInt();
		billShare = totalBill/numPeople;
		System.out.println("Each person should pay " + fmt.format(billShare));
		
		System.out.println("\nEnjoy the rest of your day!  Thank you for using this service.");
		

	}
}
