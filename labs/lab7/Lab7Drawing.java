// *********************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Darby Rupp
// CMPSC 111 Fall 2014
// Lab 7
// Date: Oct 23 2014
//
// Purpose: Create a Java Masterpiece Drawing
// ********************************

import java.awt.*;
import javax.swing.JApplet;

public class Lab7Drawing extends JApplet
{
	private final int WIDTH = 600, HEIGHT = 400;
    	Color brown = new Color(135,110,50);

	//instance vars
	int x;
	int y;
	//constructor
	public Lab7Drawing (int x1, int y1)
	{
		x = x1;
		y = y1;
	}	

	//methods
    public Lab7Drawing()
    {
    setPreferredSize(new Dimension(600, 400));
    }

    public void drawComp(Graphics comp)
    {
       	//computer monitor
    	comp.setColor(Color.gray);
    	comp.fillRect(x+75, y+75, x+420, y+220);
	//computer base
    	comp.fillRect(x+260, y+295, x+50, y+50);
    	comp.fillRect(x+200, y+345, x+170, y+10);
    	//computer screen
    	comp.setColor(Color.darkGray);
    	comp.fillRect(x+95, y+95, x+380, y+180);
    }

    public void drawDesk(Graphics desk)
    {
        //background
        desk.setColor(Color.lightGray);
        desk.fillRect(x+0, y+0, x+WIDTH, y+HEIGHT);
    	//desk
    	desk.setColor(brown);
    	desk.fillRect(x+0, y+HEIGHT-45, x+WIDTH, y+45);
    }

    public void drawScreen(Graphics screen)
    {
    	//Words; WarGames quote
    	screen.setColor(Color.green);
    	screen.drawString("Do you want to play a game?", x+115, y+120);
    	screen.drawString("\"How about... GlobalThermonuclearWar?\"", x+115, y+150);
    	screen.drawString(" - WarGames (1983)", x+215, y+190);
    	//smile
    	screen.fillOval(x+400, y+200, x+5, y+5);
    	screen.fillOval(x+420, y+200, x+5, y+5);
    	screen.drawArc(x+393, y+210, x+40, y+20, 0, -180);
    }

}
