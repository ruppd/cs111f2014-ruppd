// *********************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Darby Rupp
// CMPSC 111 Fall 2014
// Lab #
// Date: mm dd 2014
// 
// Purpose: 
// ********************************

import java.util.Date; //needed to print today's date

public class Template  //change this each lab
{
	//main method, program begins here
	public static void main(String[] args)
	{
		//Label w name and date: (change lab number)
		System.out.println("Darby Rupp\nLab 2\n" + new Date() + "\n");

		//Variable dictionary: (declare variables and use comments to explain their meanings)
	}
}
