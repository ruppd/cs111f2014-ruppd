// *********************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Darby Rupp
// CMPSC 111 Fall 2014
// Lab 2
// Date: Sep 11 2014
// 
// Purpose: Write a program to perform a simple calculation;
//          Convert from Fahrenheit to Celsius and Kelvin
// ********************************

import java.util.Date; //needed to print today's date
import java.util.Scanner;

public class Lab2
{
	//main method, program begins here
	public static void main(String[] args)
	{
		//Label w name and date:
		System.out.println("Darby Rupp\nLab 2\n" + new Date() + "\n");

		//Variables:
		Scanner input = new Scanner( System.in );
			System.out.print ("Enter Fahrenheit value: "); 			//prompt
			double fahrenheit;	
			fahrenheit = input.nextDouble();
			System.out.println("Fahrenheit value is: " + fahrenheit); 	//check	

			double celsius;
			celsius = (fahrenheit-32)*(5.0/9.0);				//compute
			System.out.println("Celsius value is: " + celsius);		//show celsius
			
			double kelvin;
			kelvin = (fahrenheit+459.67)*(5.0/9.0); 			//compute
			System.out.println("Kelvin value is: " + kelvin);		//show Kelvin
	}
}
