//Darby Rupp, CMPSC 111; Lab 1 4 Sept
//make something cool with more than 4 lines of text

import java.util.Date;

public class Lab1Part2
{
  public static void main(String[] args)
  {
    System.out.println("Darby Rupp " + new Date());
    System.out.println("Lab 1, Part 2");

    //Expressing love of a fandom - Targaryen words 'Fire and Blood'

    System.out.println("Words of House Targaryen:");
    System.out.println("    *****  ***  *****   *****");
    System.out.println("    **      *   **  **  **   ");
    System.out.println("    ****    *   ** **   **** ");
    System.out.println("    **      *   **  *   **   ");
    System.out.println("    **     ***  **   *  *****");
    System.out.println("");
    System.out.println("       *    **   **   ****  ");
    System.out.println("      ***   ***  **   ** ** ");
    System.out.println("     ** **  ** ****   **  **");
    System.out.println("     *****  **  ***   ** ** ");
    System.out.println("     ** **  **   **   ****  ");
    System.out.println("");
    System.out.println("***    **      ***    ***   ****  ");
    System.out.println("** **  **     ** **  ** **  ** ** ");
    System.out.println("***    **     ** **  ** **  **  **");
    System.out.println("** **  **     ** **  ** **  ** ** ");
    System.out.println("***    *****   ***    ***   ****  ");
  }
}
