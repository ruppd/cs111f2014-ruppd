//Darby Rupp, CMPSC 111; Lab 1 4 Sept
//Demonstrates basic structure of Java application
//ask what the first line means?

import java.util.Date;

public class Lab1
{
  public static void main(String[] args)
  {
    System.out.println("Darby Rupp " + new Date());
    System.out.println("Lab 1");

    //words of wisdom below

    System.out.println("Advice from James Gosling, creator of Java:");
    System.out.println("Don't be intimidated--give it a try!");
  }
}
