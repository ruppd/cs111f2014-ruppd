//==========================================
// Darby Rupp 
// Class Exercise
// September 29, 2014
//
// Purpose: This program uses various methods 
// of the String class
// ==========================================
public class StringExample
{

   public static void main(String args[])
   {
      // declare a variable named word of type String
      String quote;
      String quote2;

      //assign the string to the variable:
      quote = "The old that is strong does not wither.";
      quote2 = "Deep roots are not reached by the frost.";

      //print string
      System.out.println(quote);
      System.out.println(quote2);


      //perform some actions on the string:      

      //length
      int length = quote.length();
      System.out.println("Length: " + length);
      int length2 = quote2.length();
      System.out.println("Length: " + length2);

      //compare
      System.out.println("Compare: " + quote.compareTo(quote2));
      
      //change case
      System.out.println("Uppercase: " + quote.toUpperCase());
      System.out.println("Lowercase: "+quote2.toLowerCase());

      //concatenate
      System.out.println("Concat: "+quote.concat(quote2));

      //substring
      System.out.println("Substring: " +quote.substring(15));

     
   }
}
