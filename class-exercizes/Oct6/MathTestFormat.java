//==========================================
// Darby Rupp
// Class Exercise
// October 6, 2014
//
// Purpose: This program illustrates various 
// ways of formatting the output.
// ==========================================

import java.util.Scanner;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class MathTestFormat
{
  	public static void main ( String args[] )
 	{

 		double value;
 		Scanner i = new Scanner ( System.in );
 		System.out.print ( "Enter number: " );
 		value = i.nextDouble();
        	// Round the output to three decimal places
        	DecimalFormat fmt = new DecimalFormat(".###"); 				
        	System.out.println ("decimals: " +fmt.format(value));
        	NumberFormat fmt1 = NumberFormat.getPercentInstance();
        	System.out.println("percentage: " +fmt1.format(value));

		NumberFormat fmt2 = NumberFormat.getCurrencyInstance();
		System.out.println("currency: " +fmt2.format(value));
 	}
}
