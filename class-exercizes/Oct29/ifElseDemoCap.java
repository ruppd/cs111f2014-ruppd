/* CMPSC 111 Fall 2014
  Janyl Jumadinova
  
  October 29, 2014
  
  Purpose: To determine whether a grade a vowel (lower-case or upper-case)

*/

import java.util.Scanner;
public class ifElseDemoCap
{
 	public static void main ( String args[] )
 	{
		Scanner input = new Scanner ( System.in );
 		System.out.print ( "Enter a character to test: " );
 	 	char character;	  	      // new data type: char
 		character = input.next().charAt(0);	      // get character from input
		boolean isVowel;

 	 	if ((character == 'a') || (character == 'A') || (character == 'e') ||(character == 'E') || (character == 'i') || (character == 'I') || (character == 'o') || (character == 'O') || (character == 'u') || (character == 'U'))    
		// notice ' ' marks char, || is 'or', == allows for comparison (= for 
		//assignment)
		{
 		       	System.out.printf ( "%c is a vowel\n", character ); //notice %c
			isVowel = true;
		}
		else if ((character == 'y') || (character == 'Y'))
			System.out.printf ( "%c is sometimes a vowel\n", character);
 		else
 			System.out.printf ( "%c is not a vowel\n", character );

		System.out.print("Enter a word");
		String word = input.next();
		char character1 = word.charAt(0);
		char character2 = word.charAt(1);

		if ((character1 == 'a') || (character1 == 'A') || (character1 == 'e') ||(character1 == 'E') || (character1 == 'i') || (character1 == 'I') || (character1 == 'o') || (character1 == 'O') || (character1 == 'u') || (character1 == 'U'))    
		{
 		       	System.out.printf ( "%c is a vowel\n", character ); //notice %c
			isVowel = true;
		}


 	}
}

