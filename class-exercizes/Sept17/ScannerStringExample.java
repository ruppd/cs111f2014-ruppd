/* Class Exercise: Scanner and its methods
September 17, 2014
Janyl Jumadinova
*/

import java.util.Scanner;

public class ScannerStringExample
{
	public static void main(String[] args)

	{

    	// Declarations
    	Scanner in = new Scanner(System.in);
    	String string1;
    	String string2;
   	int count;
	int total;
	double num1;
	double product;
	
    	// Prompts
    	/* 
	System.out.println("Enter your favourite food");
    	string1 = in.nextLine();
    	System.out.println("Enter your favourite hobby");
    	string2 = in.nextLine();
    	System.out.println("Here is what you entered: "+string1+" and "+string2);
	*/
	
	System.out.println("Enter a count value: ");
	count = in.nextInt();
//	count--;
	total = --count;
	System.out.println("Total is " + total + "; Count is " + count);

	System.out.println("Enter another count value: ");
	count = in.nextInt();
	System.out.println("Enter a double value: ");
	num1 = in.nextDouble();
	product = num1*count;
	System.out.println("product of two numbers: " + product); //example of int getting promoted

	}
}
