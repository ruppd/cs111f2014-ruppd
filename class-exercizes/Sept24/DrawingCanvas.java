//=================================================
// Class Example using Graphics
// September 25, 2014
// Original submission by Erica Knoll in CMPSC 111, Spring 2014
// Modified by Janyl Jumadinova
// Darby Rupp
// Purpose: Program draws a tree and a sun 
//=================================================

import java.awt.*;
import javax.swing.JApplet;

public class DrawingCanvas extends JApplet
{
    //-------------------------------------------------
    // Use Graphics methods to add content to the drawing canvas
    //-------------------------------------------------
    public void paint(Graphics page)
    {
	final int WIDTH = 600;
	final int HEIGHT = 400;

	//sky
	page.setColor(Color.cyan);
	page.fillRect(0, 0, WIDTH, HEIGHT);
	//ground/snow
	page.setColor(Color.white);
	page.fillRect(0, HEIGHT-50, WIDTH, 50);
	//sun
	page.setColor(Color.yellow);
	page.fillOval(0, 0, 50, 50);
	//sun rays
	page.drawLine(25, 25, 100, 100);
	page.drawLine(25, 25, 100, 0);
	page.drawLine(25, 25, 0, 100);
	page.drawLine(25, 25, 100, 45);
	page.drawLine(25, 25, 45, 100);
	page.drawLine(25, 25, 0, 0);
	page.drawLine(25, 25, 50, 0);
	page.drawLine(25, 25, 0, 50);
	//tree
	page.setColor(Color.darkGray);
	page.fillRect(460, 230, 40, 130);
	//tree leaves
	page.setColor(Color.green);
	page.fillOval(460, 130, 125, 125);//upper right
	page.fillOval(420, 160, 120, 120);//bottom right/mid
	page.fillOval(380, 100, 120, 120);//top left
	page.fillOval(420, 110, 125, 125);//upper mid/right
	page.fillOval(390, 150, 115, 115);//lower left
    }
}
