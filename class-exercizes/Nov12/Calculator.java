public class Calculator
{
    //instance variable
    double result;
    boolean check;

    //constructor
    public Calculator(double r)
    {
        result = r;
        check = true;
    }

    //set method to calculate the result
    public void calculate (double left, char operator, double right)
    {
        switch(operator)
        {
            case '+':
                result = left + right;
                break;
            case '-':
                result = left - right;
                break;
            case '*':
                result = left * right;
                break;
            case '/':
                result = left/right;
                break;
            case '%':
                result = left%right;
                break;
            default:
                check = false;
        }
    }

    //get method to return the result
    public double getResult()
    {
        return result;
    }

    //get method to return the value check
    public boolean getCheckOperator()
    {
        return check;
    }
}
