import java.util.Scanner;

public class CalculatorMain
{
    public static void main (String args[])
    {
        //variable dictionary
        double left, right;
        char operator;
        Scanner scan = new Scanner(System.in);

        System.out.println("Enter a simple math expression: ");
        left = scan.nextDouble();
        operator = scan.next().charAt(0);
        right = scan.nextDouble();

        System.out.printf("%.2f %s %.2f = ", left, operator, right);

        Calculator calc = new Calculator(0);

        calc.calculate(left, operator, right);

        if(calc.getCheckOperator() == false)
        {
            System.out.println("Operation is not valid.");
        }
        else
        {
            System.out.printf("%.2f %n", calc.getResult());
        }
    }
}
