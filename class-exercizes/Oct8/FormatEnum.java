//==========================================
// Darby Rupp
// Class Exercise
// October 8, 2014
//
// Purpose: This program illustrates formatting
// the output with printf and using Enum.
// ==========================================

import java.util.Scanner;

public class FormatEnum
{
	public enum Day
	{
	    SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY
	}

  	public static void main ( String args[] )
 	{
		String words;
		int iValue;
		double fValue;

		Day theDay = Day.WEDNESDAY;
		Day tomorrow = Day.THURSDAY;
		System.out.println("theDay value "+theDay);
		System.out.println("theDay name "+theDay);
		System.out.println("theDay ordinal "+theDay.ordinal());
	
//		Scanner i = new Scanner ( System.in );
//		System.out.print( "Enter a word(s): ");
//		words = i.nextLine();
//		System.out.print( "Enter an integer: ");
//		iValue = i.nextInt();
//		System.out.print ( "Enter a floating point number: " );
//		fValue = i.nextDouble();

		// Round the output to three decimal places
//        	System.out.printf("%s %d %.3f %n", words, iValue, fValue);



 		}
}
