//***************************************
// CMPSC 111, Fall 2014
// Class Example
// November 5, 2014
// Purpose: A program that reads a file and displays 
// the words of that file as a list.
//*****************************************
import java.util.ArrayList;
import java.util.Scanner;
import java.io.*;

public class ArrayListEx
{
    public static void main (String args[]) throws IOException 
    {
        ArrayList <String> allWords = new ArrayList <String>();
	
	//read a text file
	File file = new File("words.txt");
	Scanner scan = new Scanner (file);
	
	//Saving words
	while(scan.hasNext()==true)
	{
	    String word = scan.next();
	    allWords.add(word);
	}
	
	// display all the words
	System.out.println(allWords);
	
	//replace 's' with 'S'
	int count = 0;
	while(count < allWords.size())
	{
	    String w = allWords.get(count);
	    if(w.contains("s"))
	    {
		w = w.replace('s', 'S');
		allWords.set(count, w);
	    }
	    count++;
	}
	System.out.println(allWords);

	//deleting words that end with 'S'
	count = 0;
	while(count<allWords.size())
	    {
		String word = allWords.get(count);
		if (word.endsWith("S"))
		{
		    allWords.remove(count);
		    count--;
		}
		count++;
	    }
	System.out.println(allWords);
    }

}
