/*
Account class that creates an Account
It has a constructor and two methods - credit and getBalance
*/

public class Account
{  
    	// instance variable
    	private double balance;
    	private String accountName;
    	private int accountNumber;

    	// constructor
    	public Account ( double initialBalance )
    	{
        	// initialize
        	balance = initialBalance;
        
    	}

	//second constructor
	public Account ( String name, int acNumber)
	{
		//initialize
		accountName = name;
		accountNumber = acNumber;
		System.out.println("Account name: "+name+"\tAccount number: "+acNumber);

    	// set method, changes the value of the variable 'balance'
    	public void credit ( double amount )
    	{
    	    balance = balance + amount;
    	}
    
    	//get method, returns the value of the variable 'balance'
    	public double getBalance ()
    	{
    	    return balance;
    	}
}
