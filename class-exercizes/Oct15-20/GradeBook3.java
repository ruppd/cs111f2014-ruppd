// Define class GradeBook with a member method displayMessage

public class GradeBook3 {
    private String courseName = "Music101"; //instance variable

    public void setCourseName (String name) {
        System.out.println("Inside setCourseName");
        courseName = name;
    }

    public String getCourseName () {
        System.out.println("Inside getCourseName");
        return courseName;
    }

    // method to display a welcome message
    public void displayMessage() {
        System.out.println("Inside displayMessage");
        System.out.printf("Welcome to the Grade Book for %s\n ",getCourseName());
    }
}
