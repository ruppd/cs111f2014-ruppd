// GradeBookTest class with argument

import java.util.Scanner;

public class GradeBookTest4 {
    public static void main (String args[]) {
        GradeBook4 gb1 = new GradeBook4 ( "CIST 1400 : Intro to Computer Programming" );
        GradeBook4 gb2 = new GradeBook4 ( "CIST 1404 : Programming Lab" );
	GradeBook4 gb3 = new GradeBook4 ( "Dragon Training 101" );
        System.out.printf ("gb1 course name is: %s\n\n",gb1.getCourseName() );
        System.out.printf ("gb2 course name is: %s\n\n",gb2.getCourseName() );
	System.out.printf ("gb3 course name is: %s\n\n",gb3.getCourseName() );

        gb1.setCourseName("blah");
        gb2.setCourseName("blah,blah");
	gb3.setCourseName("Transfiguration");
        System.out.printf ("gb1 course name is: %s\n\n",gb1.getCourseName() );
        System.out.printf ("gb2 course name is: %s\n\n",gb2.getCourseName() );
	System.out.printf ("gb3 course name is: %s\n\n",gb3.getCourseName() );

    }
}
