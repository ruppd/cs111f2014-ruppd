/* AccountTest class contains the main method.
It creates two instances of the Account object and uses
the method within the Account class
*/

import java.util.Scanner;
public class AccountTest
{  
    public static void main ( String args[] )
    {
	// create instances of Account
        Account a1 = new Account ( 50.00 );
        Account a2 = new Account ( -7.53 );
	Account a3 = new Account ("Darby Rupp", 53421);
	// Call/invoke getBalance method in Account class
        System.out.printf ("a1 balance: $%.2f\n", a1.getBalance() );
        System.out.printf ("a2 balance: $%.2f\n", a2.getBalance() );
         
        Scanner input = new Scanner ( System.in );
	// Get an input from the user
        System.out.print ( "Enter deposit for a1: ");
        double depositAmount = input.nextDouble();
        System.out.printf ( "Adding $%.2f to a1\n\n", depositAmount );
	// call/invoke credit method in Account class
        a1.credit ( depositAmount );

	// Call/invoke getBalance method in Account class
        System.out.printf ("a1 balance: $%.2f\n", a1.getBalance() );
        System.out.printf ("a2 balance: $%.2f\n", a2.getBalance() );

	// Get an input from the user
        System.out.print ( "Enter deposit for a2: ");   
        depositAmount = input.nextDouble();
        System.out.printf ( "Adding $%.2f to a2\n\n", depositAmount );
	// Call/invoke credit method in Account class
        a2.credit ( depositAmount );
	// Call/invoke getBalance method in Account class
        System.out.printf ("a1 balance: $%.2f\n", a1.getBalance() );
        System.out.printf ("a2 balance: $%.2f\n", a2.getBalance() );

	// Get an input from the user
        System.out.print ( "Enter withdrawal for a2: ");   
        double wAmount = input.nextDouble();
        System.out.printf ( "Withdrawing $%.2f to a2\n\n", wAmount );
	// Call/invoke credit method in Account class
        a2.debit ( wAmount );
	// Call/invoke getBalance method in Account class
        System.out.printf ("a1 balance: $%.2f\n", a1.getBalance() );
        System.out.printf ("a2 balance: $%.2f\n", a2.getBalance() );

    }
} 

