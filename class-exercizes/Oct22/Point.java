public class Point
{
    //instance variable
    private int x;
    private int y;

    // constructor
    public Point (int initialX, int initialY)
    {
        x = initialX;
        y = initialY;
    }

    //get methods
    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }


    //set method
    public void setX (int newX)
    {
        x = newX;
    }

    public void setY (int newY)
    {
        y = newY;
    }

    public void setXY (int newX, int newY)
    {
        x = newX;
        y = newY;
    }

}
