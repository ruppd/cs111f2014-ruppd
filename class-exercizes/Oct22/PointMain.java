public class PointMain
{
    public static void main(String[] args)
    {
        //create an instance of a Point object
        Point p1 = new Point(5,2);
        Point p2 = new Point(4,3);

        //print each point
        System.out.println("p1: "+p1.getX()+ ","+p1.getY());
        System.out.println("p2: "+p2.getX()+ ","+p2.getY());

        //change the value of a point
        p2.setXY(5,5);
        System.out.println("p2: "+p2.getX()+ ","+p2.getY());

    }
}
