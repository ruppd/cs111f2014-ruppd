public class Film
{
    private String name;
    private int length;

    //constructor
    public Film(String n, int l) {
        name = n;
        length = l;
    }

    public String getName() {
        return name;
    }

    public int getTime() {
        return length;
    }

}
