public class FilmMain
{
    public static void main(String[] args) {
        Film watch[] = new Film[4];
        watch[0] = new Film("Shrek", 133);
        watch[1] = new Film("Road to Perdition", 117);
        watch[2] = new Film("Enigma", 114);
        watch[3] = new Film("The Truth about Cats and Dogs", 93);

	//calculate the film with the longest and the shortest time
	Film longest = null, shortest = null;
	int longTime = 0, shortTime = 0;
	
	for (int i = 0; i<watch.length; i++) {
		int mins = watch[i].getTime();
		if(i==0) {
			shortest = longest = watch[i];
			shortTime = longTime = mins;
		}
		else {
			if (mins<shortTime) {
				shortTime = mins;
				shortest = watch[i];
			}
			if (mins>longTime) {
				longTime = mins;
				longest = watch[i];
			}
		}
	}
	System.out.println("The longest film is "+longest.getName()+" at "+longTime+" minutes.");
	System.out.println("The shortest film is "+shortest.getName()+" at "+shortTime+" minutes.");
    }
}
