//***************************************
// CMPSC 111, Fall 2014
// Class Example
// November 10, 2014
// Purpose: A program that uses a switch statement on a character
// to determine if a given character is a lower or an upper case vowel or not.
//*****************************************
import java.util.Scanner;
public class switchVowelCap
{
 	public static void main ( String args[] )
 	{
		Scanner input = new Scanner ( System.in );
 		System.out.print ( "Enter a character to test: " );
 	 	char character ;	  		 
		 character = input.next().charAt(0);

 		switch ( character )  	// variable for switch always int or char
 		{
			case 'a': case 'A':	// can do multiple cases (implies OR)
			case 'e': case 'E': 	
			case 'i': case 'I':	
			case 'o': case 'O': 	
			case 'u': case 'U':   	
 				System.out.printf ( "%s is a vowel\n", character );
				break;
			case 'y': case 'Y':
				System.out.printf ( "%s is sometimes a vowel\n", character );
				break;
 			default:
 				System.out.printf ( "%s not vowel\n", character );
 	 	}
 	}
}

