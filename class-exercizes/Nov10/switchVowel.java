//***************************************
// CMPSC 111, Fall 2014
// Class Example
// November 10, 2014
// Purpose: A program that uses a switch statement on a character
// to determine if a given character is a lower case vowel or not.
//*****************************************
import java.util.Scanner;
public class switchVowel
{
 	public static void main ( String args[] )
 	{
		Scanner input = new Scanner ( System.in );
 		System.out.print ( "Enter a character to test: " );
 	 	char character ;
 		character = input.next().charAt(0);
 		switch ( character )  	
 		{
		     case 'a': 	System.out.println('a');// case labels
 		     case 'e': 	System.out.println('e');// separated by :
 		     case 'i': 	System.out.println('i');// character
 		     case 'o': 	System.out.println('o');// notice use of ' '
 		     case 'u': 	System.out.println('u');// marks for char tests
 			System.out.printf ("%s is a lowercase vowel\n",character);
			break;
		    default:
 			System.out.printf ("%s not a lowercase vowel\n",character);
 	 	}
 	}
}

