//***************************************
// CMPSC 111, Fall 2014
// Class Example
// November 19, 2014
// Purpose: A program that demonstrates the 
// usage of the array and the for loop
//*****************************************
public class Array1
{
   public static void main ( String args[] )
   {
	int[] n = new int[10];
	for ( int x = 0; x < n.length; x++ )
	   n[x] = x * 10;
        System.out.println("Index   Value");
        for ( int x = 0; x < n.length; x++ )
	   System.out.println(x +"       "+n[x]);
	
	int [] grades = {82, 95, 60, 87,79, 100, 90, 89};
	for (int i=0; i < grades.length; i++)
	  System.out.println(grades[i]);
   }
}
