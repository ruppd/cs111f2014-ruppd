public class BirthdayProblem
{
    public static void main(String args[])
    {
        //initialized to false
        boolean [] birthday = new boolean[365];
        int count = 0;

        while (true)
        {
            //select a birthday at random
            int b = (int) (Math.random()*365);
            count++;
            System.out.println(b);
            if (birthday[b]==true)
                break;
            birthday[b] = true;
        }
        System.out.println("A duplicate birthday was found after "+count+" tries");

    }
}
