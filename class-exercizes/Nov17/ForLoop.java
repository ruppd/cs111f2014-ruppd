//***************************************
// CMPSC 111, Fall 2014
// Class Example
// November 17, 2014
// Purpose: A program that demonstrates the 
// usage of the for loop
//*****************************************
public class ForLoop
{
   public static void main ( String args[] )
   {
      for ( int counter = 1; counter <= 10; counter++)
      {
         System.out.println ( counter );
      }

      int sum = 0;
      for (int counter = 2; counter<=20; counter+=2)
	{
	  System.out.println(counter);
	  sum+=counter;
	}
	System.out.println("Sum is "+sum);
	
	sum = 0;
      for (int counter=20; counter>=2; counter-=2)
	{
	  System.out.println("counter 2 "+counter);
	  sum+=counter;
	}
	System.out.println("Sum 2 is "+sum);

   }
}
