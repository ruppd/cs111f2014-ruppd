//***************************************
// CMPSC 111, Fall 2014
// Class Example
// November 17, 2014
// Purpose: A program that demonstrates the 
// usage of the for loop
//*****************************************
public class NestedForLoop
{

   public static void main ( String args[] )
   {

      for ( int x = 1; x <= 3 ; x++ )
      // outer
      {
         for ( int y = 1; y <= 3 ; ++y )
         // middle
         {
            for ( int z = 1; z <= 3 ; z += 1 ) // inner 
            {
               System.out.printf ( "x = %d, y = %d, z = %d\n", x, y, z );
            }
         }
      }
   }
}

