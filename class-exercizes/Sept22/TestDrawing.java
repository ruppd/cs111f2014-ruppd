import javax.swing.*;

public class TestDrawing
{
    public static void main(String[] args)
    {
        	JFrame window = new JFrame("Darby Rupp");

      	// Add the drawing canvas and do necessary things to
     	// make the window appear on the screen!
        	window.getContentPane().add(new GraphicsInfo());
        	window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        	window.setVisible(true);
        	window.pack();
    }
}

