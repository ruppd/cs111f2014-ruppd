import javax.swing.JApplet;
import java.awt.*;

public class GraphicsInfo extends javax.swing.JApplet 
{
   	public void paint(Graphics g) 
	{
   		/* Draw the square. */
   		g.setColor(Color.blue);
   		g.fillOval(40, 40, 120, 120);
		g.setColor(Color.cyan);
		g.fillOval(40, 40, 90, 90);
		g.setColor(Color.green);
		g.fillOval(40, 40, 60, 60);
		g.fillRect(180, 40, 120, 120);
  	}  // end paint()
} // end class GraphicsInfo
