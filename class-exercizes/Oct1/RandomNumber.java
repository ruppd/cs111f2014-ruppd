//=================================================
// Class Example using Random Class
// October 1, 2014
// CMPSC 111, Fall 2014
// Darby Rupp
// 
// Purpose: Generate random integer and floating point numbers
//=================================================

import java.util.Random;


public final class RandomNumber {
  
  public static void main(String[] args)
  	{
    	int randomInt;
	float randomFloat;

    	//note a single Random object will be reused 
   		Random randomGenerator = new Random();

   		// Generate a random integer
		randomInt = randomGenerator.nextInt();
		System.out.println("A random integer: "+randomInt);

		// Generate a random integer in the range 0..10.
      	randomInt = randomGenerator.nextInt(10);
    	System.out.println("A random integer from 0 to 9: "+randomInt);


      	// Generate a random integer in the range 0..99.
      	randomInt = randomGenerator.nextInt(100);
    	System.out.println("A random integer from 0 to 99: "+randomInt);

	//Generate a random integer 1..10
	randomInt = randomGenerator.nextInt(10)+1; 
	System.out.println("A random integer from 1 to 10: "+randomInt);
   		 
	//Generate a random integer -10 .. 9
	randomInt = randomGenerator.nextInt(20)-10;
	System.out.println("A random integer from -10 to 9: "+randomInt);

	//generate a random float
	randomFloat = randomGenerator.nextFloat();
	System.out.println("A random float: "+randomFloat);

	System.out.println("A random double: "+randomGenerator.nextDouble());

	randomFloat = randomGenerator.nextFloat()*6;
	System.out.println("A random floatfrom 0 to 6: "+randomFloat);

  }
  
}
