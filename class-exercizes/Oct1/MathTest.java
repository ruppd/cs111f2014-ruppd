//=================================================
// Class Example using Math Class
// October 1, 2014
// CMPSC 111, Fall 2014
// Darby Rupp
// 
// Purpose: Experiment with various methods from the Math class
//=================================================

import java.util.Scanner;
public class MathTest
{
  	public static void main ( String args[] )
 	{
 		double value;
 		Scanner scan = new Scanner ( System.in );
 		System.out.print ( "Enter number: " );

 		value = scan.nextDouble();

 		System.out.println ("abs: "+Math.abs(value));
 		System.out.println ("ceil: "+Math.ceil(value));
 		System.out.println ("sqrt: "+Math.sqrt(value));
 		System.out.println ("Pi: "+Math.PI);
 	}
}
