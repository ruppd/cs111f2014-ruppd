
// *********************************
// Darby Rupp
// Practical 7, 6-7 Nov 2014
// Purpose: Guessing Game
// ********************************

import java.util.*;

public class GuessingGameMain
{
	//main method, program begins here
	public static void main(String[] args)
	{
		System.out.println("Darby Rupp\nPractical 7\n" + new Date() + "\n");

		//have fun
        int randomNumber, enteredNumber, count = 0;
        Random generator = new Random();
        Scanner scan = new Scanner(System.in);
	    randomNumber = generator.nextInt(100)+1;
        System.out.println("Welcome to the Guessing Game!");
        System.out.println("We have generated a random number between 1 and 100");
        System.out.println("Please input a number to guess: ");
        enteredNumber = scan.nextInt();
        count++;
        System.out.println(randomNumber);

        while(enteredNumber != randomNumber) {
            if(enteredNumber>randomNumber) {
            System.out.println("Your number is too high. Please guess again: ");
            enteredNumber = scan.nextInt();
            }
            else {
            System.out.println("Your number is too low. Please guess again:");
            enteredNumber = scan.nextInt();
            }
            count++;
        }
        System.out.println("Congratulations, the random number was "+randomNumber+"!");
        System.out.println("It took you "+count+" guesses.");
    }
}
