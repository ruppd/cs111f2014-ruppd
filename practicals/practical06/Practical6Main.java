//*****************************
// CMPSC 111
// Practical 6
// October 29, 2014
//
// Purpose: a program that determines what activities happen
// during a specific year.
//*****************************

import java.util.Date;
import java.util.Scanner;
public class Practical6Main
{
    public static void main ( String[] args)
    {
	System.out.println("Darby Rupp\nPractical 6\n" + new Date() + "\n");
        //Variable dictionary
        Scanner scan = new Scanner(System.in);
        int userInput;

        System.out.print("Please enter a year between 1000 and 3000: ");
        userInput = scan.nextInt();

        YearChecker activities = new YearChecker(userInput);

        // TO DO: include method calls
//	activities.isLeapYear();
//	activities.isCicadaYear();
//	activities.isSunspotYear();

	//leap year
	if (activities.isLeapYear()==true)
	{
		System.out.printf("%d is a Leap Year.\n", userInput);
	}
	else
	{
		System.out.printf("%d is not a Leap Year.\n", userInput);
	}

	//cicada
	if (activities.isCicadaYear()==true)
	{
		System.out.printf("%d is an emergence year for Brood II of the 17-year Cicadas.\n", userInput);
	}
	else
	{
		System.out.printf("%d is not an emergence year for Brood II of the 17-year Cicadas.\n", userInput);
	}

	//sunspot
	if (activities.isSunspotYear()==true)
	{
		System.out.printf("%d is a peak sunspot year.\n", userInput);
	}
	else
	{
		System.out.printf("%d is not a peak sunspot year.\n", userInput);
	}

	//thankyou
        System.out.println("Thank you for using this program.");
    }
}
