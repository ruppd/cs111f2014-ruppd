//====================================
// CMPSC 111 
// Practical 5
// 23-24 October 2014
//
// This program describes an octopus in the kitchen.
//====================================

import java.util.Date;

public class Practical5
{
    public static void main(String[] args)
    {
        System.out.println("Darby Rupp\n" + new Date() + "\n");

        // Variable dictionary:
        Octopus ocky;           // given octopus
        Utensil spat;           // given kitchen utensil
	Octopus drogo;		// MY octopus
	Utensil knife;		// MY kitchen utensil

        spat = new Utensil("spatula"); // create a spatula
        spat.setColor("green");        // set spatula properties--color...
        spat.setCost(10.59);           // ... and price

        ocky = new Octopus("Ocky");    // create and name the octopus
        ocky.setAge(10);               // set the octopus's age...
        ocky.setWeight(100);           // ... weight,...
        ocky.setUtensil(spat);         // ... and favorite utensil.

	knife = new Utensil("knife");
	knife.setColor("Iron");
	knife.setCost(19.96);

	drogo = new Octopus("Drogo", 32, 234);
	drogo.setUtensil(knife);

        System.out.println("Testing 'get' methods:");
        System.out.println(ocky.getName() + " weighs " +ocky.getWeight() 
            + " pounds\n" + "and is " + ocky.getAge() 
            + " years old. His favorite utensil is a "
            + ocky.getUtensil());

        System.out.println(ocky.getName() + "'s " + ocky.getUtensil() + " costs $"
            + ocky.getUtensil().getCost());
        System.out.println("Utensil's color: " + spat.getColor());

        // automatically produce a visualization of the octopus
        LJV.Context dataVisualizationContext = LJV.getDefaultContext();
        dataVisualizationContext.outputFormat = "pdf";
        dataVisualizationContext.ignorePrivateFields = false;
        LJV.drawGraph( dataVisualizationContext, ocky, "ocky-before.pdf" );

        // Use methods to change some values:
        ocky.setAge(20);
        ocky.setWeight(125);
        spat.setCost(15.99);
        spat.setColor("blue");

        System.out.println("\nTesting 'set' methods:");
        System.out.println(ocky.getName() + "'s new age: " + ocky.getAge());
        System.out.println(ocky.getName() + "'s new weight: " + ocky.getWeight());
        System.out.println("Utensil's new cost: $" + spat.getCost());
        System.out.println("Utensil's new color: " + spat.getColor());

        LJV.drawGraph( dataVisualizationContext, ocky, "ocky-after.pdf" );


	//Printing information for Drogo the Octopus:
        System.out.println("\nIntroducing Darby's Octopus Drogo:");
        System.out.println(drogo.getName() + " weighs " +drogo.getWeight() 
            + " pounds\n" + "and is " + drogo.getAge() 
            + " years old. \nHis favorite utensil is a "
            + drogo.getUtensil());

        System.out.println(drogo.getName() + "'s " + drogo.getUtensil() + " costs $"
            + drogo.getUtensil().getCost());
        System.out.println("Utensil's color: " + knife.getColor()+"\n");

        LJV.drawGraph( dataVisualizationContext, drogo, "drogo.pdf" );

    }
}
