
// *********************************
// Darby Rupp
// Practical 2, 11-12 Sept 2014
// Purpose: Create a Mad Lib (with calculation)
// ********************************

import java.util.Date; //needed to print today's date
import java.util.Scanner;

public class MadLib
{
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);

		System.out.println("Darby Rupp\nPractical 3\n" + new Date() + "\n");

		//declare variables
		String noun;
		String noun2;
		String home;
		String day;
		String adjective;
		String verb;
		String place;
		String item;
		String adjective2;
		String verb2;
		String adverb;
			

		//program
		//prompts
		System.out.print("Enter a singular noun: "); //title1
		noun = in.nextLine();
		System.out.print("Enter another singular noun: "); //title2
		noun2 = in.nextLine();
		System.out.print("Enter yet another singular noun: "); //home
		home = in.nextLine();
		System.out.print("Enter a day of the week: ");
		day = in.nextLine();
		System.out.print("Enter an adjective: ");
		adjective = in.nextLine();
		System.out.print("Enter a verb: "); //action
		verb = in.nextLine();
		System.out.print("Enter a place name: ");
		place = in.nextLine();
		System.out.print("Enter an item: ");
		item = in.nextLine();
		System.out.print("Enter another adjective: ");
		adjective2 = in.nextLine();
		System.out.print("Enter another verb: ");
		verb2 = in.nextLine();
		System.out.print("Enter an adverb: ");
		adverb = in.nextLine();
		

		//story
		System.out.println("\nThe \033[1m"+noun+"\033[0m and the \033[1m"+noun2+"\033[0m.\n");
		System.out.print("Once upon a time, there was a \033[1m"+noun+"\033[0m and a \033[1m"+noun2+"\033[0m.  ");
		System.out.print("They lived in a \033[1m"+home+"\033[0m.  ");
		System.out.print("On one fine \033[1m"+day+"\033[0m, both the \033[1m"+noun+"\033[0m and the \033[1m"+noun2+"\033[0m were feeling \033[1m"+adjective+"\033[0m, ");
		System.out.print("so they decided to \033[1m"+verb+"\033[0m to the \033[1m"+place+"\033[0m.  ");
		System.out.print("At the \033[1m"+place+"\033[0m, they bought a \033[1m"+item+"\033[0m, which they brought back to their \033[1m"+home+"\033[0m.  ");
		System.out.print("Now they were feeling \033[1m"+adjective2+"\033[0m, so they \033[1m"+verb2+"ed\033[0m in to bed and lived \033[1m"+adverb+"\033[0m ever after.\n");

	}
}
