
// *********************************
// Darby Rupp
// Practical 2, 11-12 Sept 2014
// Purpose: Print something "Interesting"
//          Print Disney Castle
// ********************************

import java.util.Date; //needed to print today's date

public class Practical02
{
	public static void main(String[] args)
	{
		//Label w name and date:
		System.out.println("Darby Rupp\nCMPS 111, Practical 2\n" + new Date() + "\n");

		//Disney Castle
		System.out.println("           ~*\n            ^");
		System.out.println("           / \\ \n          *( )");
		System.out.println("    ~*    ^/ \\ ");
		System.out.println("    / \\ ^|    |* ~* \n ~* | |/      \\^ /\\ ~* ");
		System.out.println(" /\\ /           \\|| ^");
		System.out.println("/         _       \\/ \\ \n)       /   \\        (");
		System.out.println("|      (     )       |");
		System.out.println("|_______\\   /________|");
	}
}
