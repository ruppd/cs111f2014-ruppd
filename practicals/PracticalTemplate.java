// *********************************
// Darby Rupp
// Practical 2, 11-12 Sept 2014
// Purpose: Print something "Interesting
// ********************************

import java.util.Date; //needed to print today's date

public class Template
{
	//main method, program begins here
	public static void main(String[] args)
	{
		//Label w name and date: (change practical number)
		System.out.println("Darby Rupp\nPractical 2\n" + new Date() + "\n");

		//have fun
	}
}
