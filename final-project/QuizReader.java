/*============================================================================================================
This program has the following methods:

.readQuestion(questionNumber) 	=> String
	used in CHARACTERQUIZMAIN

.getAnswers(questionNumber) 		=> array 
	used in AnswerRandomizer method .answersRandomOrder(questionNumber) => array
	used in CharacterReader method  .getNickAnswers()		\
									.getBaronAnswers()		 |
									.getLadyAnswers()		 |
									.getFriarAnswers()		  ===> all return array
									.getPeevesAnswers()		 |
									.getMyrtleAnswers()		 |
									.getBinnsAnswers()		/

This program compiles correctly.
============================================================================================================*/

import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;

public class QuizReader {

    private int questionNumber;
	private int a0, a1, a2, a3, a4, a5, a6;
	

//constructor, takes no parameters, initializes all to zero
	public QuizReader () {
	    questionNumber = 0;
		a0 = 0;
		a1 = 0;
		a2 = 0;
		a3 = 0;
		a4 = 0;
		a5 = 0;
		a6 = 0;
	}


//.readQuestion(questionNumber); returns String;
	public String readQuestion(int questionNumber) throws IOException {
	    Scanner scan = new Scanner(new File("PersonalityQuizQuestions.txt"));
	    String questions [] = new String [15]; //array 'questions', 0-14 ...creates new array every time called?
            for (int qN=0; qN<questions.length; qN++) {
                questions[qN] = scan.nextLine();
            }
            return questions[questionNumber]; //returns just the one question
	}


//.getAnswer(questionNumber);
	public String[] getAnswers(int questionNumber) throws IOException {
	    //create Array for Answers
	    String answers [] = new String [7];
	    
	    //different question numbers
	    if (questionNumber == 0) {
			//assigns strings in answers[] for individual question numbers 
		    Scanner q1scan = new Scanner(new File("Question1Answers.txt"));
                for (int count=0; count<answers.length; count++) {
            	    answers[count] = q1scan.nextLine();
                }
	    }
	    else if (questionNumber == 1) {
			Scanner q2scan = new Scanner(new File("Question2Answers.txt"));
                for (int count=0; count<answers.length; count++) {
            	    answers[count] = q2scan.nextLine();
                }
	    }
	    else if (questionNumber == 2) {
			Scanner q3scan = new Scanner(new File("Question3Answers.txt"));
                for (int count=0; count<answers.length; count++) {
            	    answers[count] = q3scan.nextLine();
                }
	    }
	    else if (questionNumber == 3) {
			Scanner q4scan = new Scanner(new File("Question4Answers.txt"));
                for (int count=0; count<answers.length; count++) {
            	    answers[count] = q4scan.nextLine();
                }
	    }
	    else if (questionNumber == 4) {
			Scanner q5scan = new Scanner(new File("Question5Answers.txt"));
                for (int count=0; count<answers.length; count++) {
            	    answers[count] = q5scan.nextLine();
                }
	    }
	    else if (questionNumber == 5) {
			Scanner q6scan = new Scanner(new File("Question6Answers.txt"));
                for (int count=0; count<answers.length; count++) {
            	    answers[count] = q6scan.nextLine();
                }
	    }
	    else if (questionNumber == 6) {
			Scanner q7scan = new Scanner(new File("Question7Answers.txt"));
                for (int count=0; count<answers.length; count++) {
            	    answers[count] = q7scan.nextLine();
                }
	    }
	    else if (questionNumber == 7) {
			Scanner q8scan = new Scanner(new File("Question8Answers.txt"));
                for (int count=0; count<answers.length; count++) {
            	    answers[count] = q8scan.nextLine();
                }
	    }
	    else if (questionNumber == 8) {
			Scanner q9scan = new Scanner(new File("Question9Answers.txt"));
                for (int count=0; count<answers.length; count++) {
            	    answers[count] = q9scan.nextLine();
                }
	    }
	    else if (questionNumber == 9) {
			Scanner q10scan = new Scanner(new File("Question10Answers.txt"));
                for (int count=0; count<answers.length; count++) {
            	    answers[count] = q10scan.nextLine();
                }
	    }
	    else if (questionNumber == 10) {
			Scanner q11scan = new Scanner(new File("Question11Answers.txt"));
                for (int count=0; count<answers.length; count++) {
            	    answers[count] = q11scan.nextLine();
                }
	    }
	    else if (questionNumber == 11) {
			Scanner q12scan = new Scanner(new File("Question12Answers.txt"));
                for (int count=0; count<answers.length; count++) {
            	    answers[count] = q12scan.nextLine();
                }
	    }
	    else if (questionNumber == 12) {
			Scanner q13scan = new Scanner(new File("Question13Answers.txt"));
                for (int count=0; count<answers.length; count++) {
            	    answers[count] = q13scan.nextLine();
                }
	    }
	    else if (questionNumber == 13) {
			Scanner q14scan = new Scanner(new File("Question14Answers.txt"));
                for (int count=0; count<answers.length; count++) {
            	    answers[count] = q14scan.nextLine();
                }
	    }
	    else if (questionNumber == 14) {
			Scanner q15scan = new Scanner(new File("Question15Answers.txt"));
                for (int count=0; count<answers.length; count++) {
            	    answers[count] = q15scan.nextLine();
                }
	    }
		//returns entire array
		return answers;
	}

}
