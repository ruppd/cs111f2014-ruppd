/*=================================================
This program has the following methods:

.saveUserAnswer(int questionNumber) 	=> VOID
	used in CHARACTERQUIZMAIN

.gerUserAnswers() 		=> array 
	used in CHARACTERQUIZMAIN

This program uses the following methods:

This program compiles correctly and is complete
=================================================*/

import java.util.ArrayList;
import java.util.Scanner;
import java.io.IOException;

public class CharacterCreator {

	private int questionNumber;
	String userAnswers [] = new String [15];

	public CharacterCreator() {
		questionNumber = 0;
	}

	public void saveUserAnswer(int questionNumber, String[] a) {
		System.out.print("Enter number 0-6 which corresponds with your answer choice: ");
		Scanner input = new Scanner(System.in);
		int i = input.nextInt();
		userAnswers[questionNumber] = a[i];		
	}

	public String[] getUserAnswers() {
	return userAnswers;
	}


}
