public class CharacterAttribute {

    private int id;
    private static int nextId = 0;
    private String identifier;
    private int questionNumber;
    private String answer;
//    private boolean match;

    public CharacterAttribute(String ident, int qN, String a) {
        id = nextId;
        nextId++;
        identifier = ident;
        questionNumber = qN;
//        done = false;
    }


    public String getIdentifier() {
        return identifier;
    }

    public int getQuestionNumber() {
        return questionNumber;
    }

    public String getAnswer() {
        return answer;
    }


//    public void markDone() {
//        done = true;
//    }

//    public boolean isDone() {
//        return done;
//    }

//    public String toString() {
//        return new String(id + ", " + priority + ", " + category + ", " + task + ", done? " + done);
//    }

}
