import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;


public class CharacterLists {

    private String character, identifier;
    private ArrayList<CharacterAttribute> characterAttributes;
    private static final String QUIZCHARACTERS = "PersonalityQuizCharacters.txt";
    private static final String QUIZQUESTIONS = "PersonalityQuizQuestions.txt";


    public CharacterLists () {
        CharacterAttribute = new ArrayList<CharacterAttributte>();
	character = identifier;
	
    }


    public void readCharacters() {
        Scanner characterScanner = new Scanner(new File(QUIZCHARACTERS)); //reads file
	//reads items one at a time, assigns identifier, questionNumber, and answer
        while(characterScanner.hasNext()) {
            String characterAttribute = characterScanner.nextLine();
            Scanner attributeScanner = new Scanner(characterAttribute);
            attributeScanner.useDelimiter(",");
            String identifier;
	    String questionNumber;
	    String answer;
            identifier = attributeScanner.next();
            questionNumber = attributeScanner.next();
            answer = attributeScanner.next();
            CharacterAttribute characterAttribute = new CharacterAttribute(identifier, questionNumber, answer);
            characterAttribute.add(characterAttribute);
	}
    }

    public Iterator createCharacter (String identifier) {
        ArrayList<CharacterAttribute> c = new ArrayList<CharacterAttribute>();
        Iterator iterator = characterAttribute.iterator();
        while(iterator.hasNext()) {
	    CharacterAttribute characterAttribute = (CharacterAttribute)iterator.next();
	    if(characterAttribute.getIdentifier().equals(identifier)) {
		c.add(characterAttribute);
	    }
	}
        return c.iterator();
    }


    public void answersInRandomOrder(int questionNumber) {

    }

    public void assignAnswerToUser() {

    }

    public void countNick() {

    }

    public void countLady() {

    }

    public void countBaron() {

    }

    public void countFriar() {

    }

    public void countBinns() {

    }

    public void countMyrtle() {

    }

}
