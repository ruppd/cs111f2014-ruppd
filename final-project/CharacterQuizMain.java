/*=================================
This program compiles correctly.
=================================*/

import java.util.Iterator;
import java.util.Scanner;
import java.io.IOException;


public class CharacterQuizMain {

    public static void main(String[] args) throws IOException {
		int questionNumber = 0;
        System.out.println("Which Harry Potter Ghost are you? Take quiz to find out!");
	
		QuizReader quiz = new QuizReader();
		AnswerRandomizer random = new AnswerRandomizer(questionNumber);
		CharacterCreator ccreate = new CharacterCreator();


		for (questionNumber=0; questionNumber<=14; questionNumber++) {
			//prints question
			System.out.println();
		    System.out.println(quiz.readQuestion(questionNumber));

		    //prints answers in a random order
			quiz.getAnswers(questionNumber);
		    String [] a = random.answersRandomOrder(questionNumber);
		    System.out.println("\t0 "+a[0]);
		    System.out.println("\t1 "+a[1]);	
		    System.out.println("\t2 "+a[2]);
		    System.out.println("\t3 "+a[3]);
		    System.out.println("\t4 "+a[4]);
		    System.out.println("\t5 "+a[5]);
		    System.out.println("\t6 "+a[6]);

			//allows user to choose answer
			//assigns user answer 
			ccreate.saveUserAnswer(questionNumber,a);
		}

	//figuring out and printing character!
		CharacterComparison ccomp = new CharacterComparison();
		//increase efficiency by assigning things instead of calling methods a million times.
		int countNick, countBaron, countFriar, countBinns, countLady, countPeeves, countMyrtle;
		countNick = ccomp.compareToNick(ccreate);
		countBaron = ccomp.compareToBaron(ccreate);
		countFriar = ccomp.compareToFriar(ccreate);
		countBinns = ccomp.compareToBinns(ccreate);
		countLady = ccomp.compareToLady(ccreate);
		countPeeves = ccomp.compareToPeeves(ccreate);
		countMyrtle = ccomp.compareToMyrtle(ccreate);

		//takes into account ties (if __ is greatest count, and if greatest number equals
		//another character's count, increase it by one.
		//done in order so all ties will be resolved Nick>Peeves>Friar>Lady>Baron>Myrtle>Binns
		if ( (countNick>=countBaron && countNick>=countLady &&
			countNick>=countFriar && countNick>=countPeeves &&
			countNick>=countBinns && countNick>=countMyrtle ) &&
			(countNick==countBaron || countNick==countLady || countNick==countFriar ||
			countNick==countBinns || countNick==countPeeves || countNick==countMyrtle) ) {
		countNick++;
		}

		if ( (countPeeves>=countBaron && countPeeves>=countLady &&
			countPeeves>=countFriar && countPeeves>=countNick &&
			countPeeves>=countBinns && countPeeves>=countMyrtle ) &&
			(countPeeves==countBaron || countPeeves==countLady || countPeeves==countFriar ||
			countPeeves==countBinns || countPeeves==countNick || countPeeves==countMyrtle) ) {
		countPeeves++;
		}

		if ( (countFriar>=countBaron && countFriar>=countLady &&
			countFriar>=countNick && countFriar>=countPeeves &&
			countFriar>=countBinns && countFriar>=countMyrtle ) &&
			(countFriar==countBaron || countFriar==countLady || countFriar==countNick ||
			countFriar==countBinns || countFriar==countPeeves || countFriar==countMyrtle) ) {
		countFriar++;
		}

		if ( (countLady>=countBaron && countLady>=countFriar && 
			countLady>=countNick && countLady>=countPeeves &&
			countLady>=countBinns && countLady>=countMyrtle ) &&
			(countLady==countBaron || countLady==countFriar || countLady==countNick ||
			countLady==countBinns || countLady==countPeeves || countLady==countMyrtle) ) {
		countLady++;
		}

		if ( (countBaron>=countNick && countBaron>=countLady &&
			countBaron>=countFriar && countBaron>=countPeeves &&
			countBaron>=countBinns && countBaron>=countMyrtle ) &&
			(countBaron==countNick || countBaron==countLady || countBaron==countFriar ||
			countBaron==countBinns || countBaron==countPeeves || countBaron==countMyrtle) ) {
		countBaron++;
		}

		if ( (countMyrtle>=countBaron && countMyrtle>=countLady &&
			countMyrtle>=countFriar && countMyrtle>=countPeeves &&
			countMyrtle>=countBinns && countMyrtle>=countNick ) &&
			(countMyrtle==countBaron || countMyrtle==countLady || countMyrtle==countFriar ||
			countMyrtle==countBinns || countMyrtle==countPeeves || countMyrtle==countNick) ) {
		countMyrtle++;
		}

		if ( (countBinns>=countBaron && countBinns>=countLady &&
			countBinns>=countFriar && countBinns>=countPeeves &&
			countBinns>=countNick && countBinns>=countNick ) &&
			(countBinns==countBaron || countBinns==countLady || countBinns==countFriar ||
			countBinns==countNick || countBinns==countPeeves || countBinns==countNick) ) {
		countBinns++;
		}

		//compares all user answers and calculates which character is most similar
		// if Nick answers >= all other answers, prints Nearly Headless Nick and descr.
		if (countNick>=countBaron && countNick>=countLady &&
			countNick>=countFriar && countNick>=countPeeves &&
			countNick>=countBinns && countNick>=countMyrtle ) {
				System.out.println("\nYou are \033[1mNearly Headless Nick\033[0m!");
				System.out.println("Like the House Ghost of Gryffindor, you are the epitome of chilvarly, and are driven by the desire to do what is right.  However, try not to reach too far beyond your abilities or circumstances, this will only cause you neck pain or heartache.");//add description!
		}
		// if Bloody Baron answers >= all other answers, prints Bloody Baron and descr.
		if (countBaron>=countNick && countBaron>=countFriar &&
			countBaron>=countLady && countBaron>=countPeeves &&
			countBaron>=countBinns&& countBaron>=countMyrtle ) {
				System.out.println("\nYou are \033[1mthe Bloody Baron\033[0m!");
				System.out.println("Like the House Ghost of Slytherin, at heart you're a romantic, though looks may be deceiving. Quick to act, try to keep your temper in check, otherwise you may do something you'll regret for the rest of your existence.");//add description!
		}
		// if Lady answers >= all other answers, prints Lady and descr.
		if (countLady>=countBaron && countLady>=countNick &&
			countLady>=countFriar && countLady>=countPeeves &&
			countLady>=countBinns && countLady>=countMyrtle ) {
				System.out.println("\nYou are \033[1mthe Grey Lady\033[0m!");
				System.out.println("Like the House Ghost of Ravenclaw, your desire to excel is evident, and because of this drive you are incredibly independent.  However, be mindful  of how your actions may affect others, and don't let your pride get in the way of rebuilding relationships.");//add description!
		}
		// if Friar answers >= all other answers, prints Friar and descr.
		if (countFriar>=countBaron && countFriar>=countLady &&
			countFriar>=countNick  && countFriar>=countPeeves &&
			countFriar>=countBinns && countFriar>=countMyrtle ) {
				System.out.println("\nYou are \033[1mthe Fat Friar\033[0m!");
				System.out.println("Like the House Ghost of Hufflepuff, generally you are a happy and forgiving person, and are always the optimist!  Even if you don't get the position you want, you'll stil be able to keep your cheerful disposition, and those around you will be grateful for your refreshing kindness.");//add description!
		}
		// if Peeves answers >= all other answers, prints Peeves and descr.
		if (countPeeves>=countBaron && countPeeves>=countLady &&
			countPeeves>=countFriar && countPeeves>=countNick &&
			countPeeves>=countBinns && countPeeves>=countMyrtle ) {
				System.out.println("\nYou are \033[1mPeeves\033[0m!");
				System.out.println("This poltergeist is not truly a ghost, but since when have the rules mattered to you?  Always a trickster, you enjoy messing with the status quo and keeping people on their toes, especially people in positions of authority.  Your friends see you as a fun-loving party-goer, and are always ready to have a good time when they hang out with you.");//add description!
		}
		// if Myrtle answers >= all other answers, prints Myrtle and descr.
		if (countMyrtle>=countBaron && countMyrtle>=countLady &&
			countMyrtle>=countFriar && countMyrtle>=countPeeves &&
			countMyrtle>=countBinns && countMyrtle>=countNick ) {
				System.out.println("\nYou are \033[1mMoaning Myrtle\033[0m!");
				System.out.println("You are very sensitive and put a great deal of merit in what others have to say, particularly when it pertains to you.  Try not to let negative comments get to you.");//add description!
		}
		// if Binns answers >= all other answers, prints Binns and descr.
		if (countBinns>=countBaron && countBinns>=countLady &&
			countBinns>=countFriar && countBinns>=countPeeves &&
			countBinns>=countNick  && countBinns>=countMyrtle ) {
				System.out.println("\nYou are \033[1mProfessor Binns!\033[0m");
				System.out.println("Though many people don't like learning about your area of expertise, you're always eager to share your knowledge and expand the minds of those around you.  Be careful of oversleeping.");//add description!
		}


    }

}
