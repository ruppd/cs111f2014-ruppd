/*============================================================================================================
This program has the following methods:

.answersRandomOrder(int questionNumber) 	=> array
	used in CHARACTERQUIZMAIN

This program uses the following methods:
from QuizReader
	.getAnswers(questionNumber) 

This program compiles correctly BUT IS NOT PROPERLY RANDOMIZING
============================================================================================================*/

import java.util.ArrayList;
import java.util.Random;
import java.io.IOException;

public class AnswerRandomizer {

    private int questionNumber;
	private int a0, a1, a2, a3, a4, a5, a6;
	

	public AnswerRandomizer (int qN) {
	    questionNumber = qN;
		a0 = 0;
		a1 = 0;
		a2 = 0;
		a3 = 0;
		a4 = 0;
		a5 = 0;
		a6 = 0;
	}

	public String[] answersRandomOrder(int questionNumber) throws IOException {
		QuizReader quiz = new QuizReader();
		quiz.getAnswers(questionNumber);
	    //create random order
		Random r = new Random ();
		a0 = r.nextInt(7); //0-6
		a1 = r.nextInt(7);
		while (a1 == a0) {
			a1 = r.nextInt(7);
		}
		a2 = r.nextInt(7);
		while (a2 == a1 || a2 == a0) {
			a2 = r.nextInt(7);
		}
		a3 = r.nextInt(7);
		while (a3 == a2 || a3 == a1 || a3 == a0) {
			a3 = r.nextInt(7);
		}
		a4 = r.nextInt(7);
		while ( a4 == a3 || a4 == a2 || a4 == a1 || a4 == a0) {
			a4 = r.nextInt(7);
		}
		a5 = r.nextInt(7);
		while ( a5 == a4 || a5 == a3 || a5 == a2 || a5 == a1 || a5 == a0) {
			a5 = r.nextInt(7);
		}
		a6 = r.nextInt(7);
		while ( a6 == a5 || a6 == a4 || a6 == a3 || a6 == a2 || a6 == a1 || a6 == a0) {
			a6 = r.nextInt(7);
		}
		
		String inRandomOrder [] = new String [7];
		inRandomOrder[0] = quiz.getAnswers(questionNumber)[a0];
		inRandomOrder[1] = quiz.getAnswers(questionNumber)[a1];
		inRandomOrder[2] = quiz.getAnswers(questionNumber)[a2];
		inRandomOrder[3] = quiz.getAnswers(questionNumber)[a3];
		inRandomOrder[4] = quiz.getAnswers(questionNumber)[a4];
		inRandomOrder[5] = quiz.getAnswers(questionNumber)[a5];
		inRandomOrder[6] = quiz.getAnswers(questionNumber)[a6];
		return inRandomOrder;
	}
}
