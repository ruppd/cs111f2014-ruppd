/*=========================================================================
This program has the following methods:

.getNickAnswers()	 					=> array
	used in CharacterComparison method .compareToNick() 	=> int
.getLadyAnswers() 						=> array
	used in CharacterComparison method .compareToLady() 	=> int
.getBaronAnswers() 						=> array
	used in CharacterComparison method .compareToBaron()	=> int
.getFriarAnswers() 						=> array
	used in CharacterComparison method .compareToFriar()	=> int
.getPeevesAnswers() 					=> array
	used in CharacterComparison method .compareToPeeves()	=> int
.getMyrtleAnswers() 					=> array
	used in CharacterComparison method .compareToMyrtle()	=> int
.getBinnsAnswers()	 					=> array
	used in CharacterComparison method .compareToBinns()	=> int



This program uses the following methods:
from QuizReader
	.getAnswers(questionNumber) => array

This program compiles correctly!
=========================================================================*/

import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;

public class CharacterReader {

	private int questionNumber;

	public CharacterReader() throws IOException {
		questionNumber = 0;
	}

	public String[] getNickAnswers() throws IOException {
		QuizReader quiz = new QuizReader(); 
		//QuizReader needed to .getAnswers()
		String NickAnswers [] = new String [15];
		//^creates String Array with 15 spots
        for (int qN=0; qN<NickAnswers.length; qN++) {
            NickAnswers[qN] = quiz.getAnswers(qN)[0];
			//^saves Nick's 15 answers
        }
		return NickAnswers;
	}

	public String[] getBaronAnswers() throws IOException {
		QuizReader quiz = new QuizReader();
		String BaronAnswers [] = new String [15];
        for (int qN=0; qN<BaronAnswers.length; qN++) {
            BaronAnswers[qN] = quiz.getAnswers(qN)[1];
        }
		return BaronAnswers;
	}

	public String[] getLadyAnswers() throws IOException {
		QuizReader quiz = new QuizReader();
		String LadyAnswers [] = new String [15];
        for (int qN=0; qN<LadyAnswers.length; qN++) {
            LadyAnswers[qN] = quiz.getAnswers(qN)[2];
        }
		return LadyAnswers;
	}

	public String[] getFriarAnswers() throws IOException {
		QuizReader quiz = new QuizReader();
		String FriarAnswers [] = new String [15];
        for (int qN=0; qN<FriarAnswers.length; qN++) {
            FriarAnswers[qN] = quiz.getAnswers(qN)[3];
        }
		return FriarAnswers;
	}

	public String[] getPeevesAnswers() throws IOException {
		QuizReader quiz = new QuizReader();
		String PeevesAnswers [] = new String [15];
        for (int qN=0; qN<PeevesAnswers.length; qN++) {
            PeevesAnswers[qN] = quiz.getAnswers(qN)[4];
        }
		return PeevesAnswers;
	}

	public String[] getMyrtleAnswers() throws IOException {
		QuizReader quiz = new QuizReader();
		String MyrtleAnswers [] = new String [15];
        for (int qN=0; qN<MyrtleAnswers.length; qN++) {
            MyrtleAnswers[qN] = quiz.getAnswers(qN)[5];
        }
		return MyrtleAnswers;
	}

	public String[] getBinnsAnswers() throws IOException {
		QuizReader quiz = new QuizReader();
		String BinnsAnswers [] = new String [15];
        for (int qN=0; qN<BinnsAnswers.length; qN++) {
            BinnsAnswers[qN] = quiz.getAnswers(qN)[6];
        }
		return BinnsAnswers;
	}

}
