/*==============================================
This program has the following methods:

.compareToNick() 	=> int
	used in MAIN
.compareToLady() 	=> int
	used in MAIN
.compareToBaron() 	=> int
	used in MAIN
.compareToFriar() 	=> int
	used in MAIN
.compareToPeeves() 	=> int
	used in MAIN
.compareToMyrtle() 	=> int
	used in MAIN
.compareToBinns() 	=> int
	used in MAIN

This program uses the following methods:
from CharacterCreator
	.getUserAnswers() 	=> array
from CharacterReader
	.getNickAnswers() 	=> array
	.getLadyAnswers() 	=> array
	.getBaronAnswers() 	=> array
	.getFriarAnswers() 	=> array
	.getPeevesAnswers() 	=> array
	.getMyrtleAnswers() 	=> array
	.getBinnsAnswers() 	=> array

This program compiles correctly!
==============================================*/

import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;

public class CharacterComparison {

	int countNick, countLady, countBaron, countFriar, countBinns, countMyrtle, countPeeves;
	private int questionNumber;


	public CharacterComparison() {
		questionNumber = 0;
		countNick = 0;
		countLady = 0;
		countBaron = 0;
		countFriar = 0;
		countBinns = 0;
		countMyrtle = 0;
		countPeeves = 0;
	}

	public int compareToNick(CharacterCreator cc) throws IOException {
		CharacterReader cr = new CharacterReader(); 	//contains method to get character's answers
		for (int count = 0; count<15; count++) {	//goes through all answers and compares if they are similar to specific character's
			if (cc.getUserAnswers()[count].equals(cr.getNickAnswers()[count])) {
				countNick++;
			}
		}
		return countNick;				//number of similar answers
	}

	public int compareToBaron(CharacterCreator cc) throws IOException {	//same as prev
		CharacterReader cr = new CharacterReader();
		for (int count = 0; count<15; count++) {
			if (cc.getUserAnswers()[count].equals(cr.getBaronAnswers()[count])) {
				countBaron++;
			}
		}
		return countBaron;
	}

	public int compareToLady(CharacterCreator cc) throws IOException {	//same as prev
		CharacterReader cr = new CharacterReader();
		for (int count = 0; count<15; count++) {
			if (cc.getUserAnswers()[count].equals(cr.getLadyAnswers()[count])) {
				countLady++;
			}
		}
		return countLady;
	}

	public int compareToFriar(CharacterCreator cc) throws IOException {	//same as prev
		CharacterReader cr = new CharacterReader();
		for (int count = 0; count<15; count++) {
			if (cc.getUserAnswers()[count].equals(cr.getFriarAnswers()[count])) {
				countFriar++;
			}
		}
		return countFriar;
	}

	public int compareToPeeves(CharacterCreator cc) throws IOException {	//same as prev
		CharacterReader cr = new CharacterReader();
		for (int count = 0; count<15; count++) {
			if (cc.getUserAnswers()[count].equals(cr.getPeevesAnswers()[count])) {
				countPeeves++;
			}
		}
		return countPeeves;
	}

	public int compareToMyrtle(CharacterCreator cc) throws IOException {	//same as prev
		CharacterReader cr = new CharacterReader();
		for (int count = 0; count<15; count++) {
			if (cc.getUserAnswers()[count].equals(cr.getMyrtleAnswers()[count])) {
				countMyrtle++;
			}
		}
		return countMyrtle;
	}

	public int compareToBinns(CharacterCreator cc) throws IOException {	//same as prev
		CharacterReader cr = new CharacterReader();
		for (int count = 0; count<15; count++) {
			if (cc.getUserAnswers()[count].equals(cr.getBinnsAnswers()[count])) {
				countBinns++;
			}
		}
		return countBinns;
	}

}
